import { Entity, PrimaryGeneratedColumn, Column, EntitySchema } from "typeorm"

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    login: string

    @Column()
    name: string

    @Column()
    password: string

}
