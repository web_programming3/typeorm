import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "Espesso";
    // product.price = 60;
    // await productRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    console.log("Loading product from the database...")
    const products = await productRepository.find()
    console.log("Loaded product: ", products)

    console.log("Here you can setup and run express / fastify / any other framework.")
    
   const updatedProduct = await productRepository.findOneBy({id:1})
   console.log(updatedProduct);
   updatedProduct.price = 90;
   await productRepository.save(updatedProduct)

}).catch(error => console.log(error))
